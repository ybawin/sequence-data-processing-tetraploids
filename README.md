# Sequence data processing of tetraploid samples

## Description

This project contains python scripts that can be used to facilitate the processing of sequence data from tetraploid samples. Two scripts are included:

1. **ReadAssignmentTetraploids.py** assigns reads to one of the subgenomes of a tetraploid genome based on their characteristics when mapped onto each subgenome separately.
2. **FilterVCF.py** filters a VCF file to obtain a set of genotype calls that fulfill the predefined filter criteria regarding the read depth of genotype calls, the quality score of genotype calls and SNPs, the proportion of heterozygous genotype calls per SNP, and the data completeness.

## Citation



## Requirements

The python scripts were developed in python3.6. Further requirements are listed, tagged per component: <sup>1</sup>ReadAssignmentTetraploids.py, <sup>2</sup>FilterVCF.py.

os<sup>1,2</sup>, sys<sup>1,2</sup>, argparse<sup>1,2</sup>, datetime<sup>1,2</sup>, multiprocessing<sup>1</sup>, numpy<sup>1</sup>, pysam<sup>1</sup>, pandas<sup>1</sup>.

The script ReadAssignmentTetraploids.py also uses Picard (Picard Toolkit, 2019) to add a read group to the output bam files.

## Building and installing

### 1. ReadAssignmentTetraploids.py

The script ReadAssignmentTetraploids.py compares the mapping characteristics of each read in two bam files (each containing the same reads mapped onto a different subgenome of a tetraploid reference genome sequence) to assign the read to its most likely subgenome of origin. Two input bam files are required for each sample: a bam file containing all reads from subgenome A and B mapped onto subgenome A (bam file *A*), and a second bam file containing all reads from subgenome A and B mapped onto subgenome B (bam file *B*). Bam file *A* and *B* must have the same file name (ending on .bam) and all bam files *A* must be stored in a different directory (dir_A) than all bam files *B* (dir_B). The script was developed with input bam files created with the BWA software (Li & Durbin, 2009).

Before reads can be assigned to a subgenome, the input bam files are first filtered for secondary alignments and the reads in each bam file are resorted on read ID. The bam files without secondary alignments are saved in a new subdirectory ('woSecAl'), while the resorted bam files are subsequently deposited in a second new subdirectory ('sorted'). The script will only create a new bam file in 'woSecAl' or in 'sorted' if no bam file with the same name is present in these directories. Afterwarts, reads are assigned to one subgenome if:

1) A read is mapped onto one subgenome but not onto the other.
2) A read is mapped onto both subgenomes but its mismatch score with one subgenome is lower than its mismatch score with the other.

The mismatch score of a read with a subgenome is calculated as the sum of the number of mismatches between the read and its mapped region in the subgenome. Three different types of mismatches are considered: point mutations (substitution of one nucleotide by another), indel mutations (inserted or deleted nucleotides), and clipping artefacts (hard- or soft-clipped nucleotides at the 5' or 3'-end of a read). The impact of indel mutations and clipping artefacts on the mismatch score can be adjusted via the options --indel_weight and --clipping_weight, respectively. These options specify a factor that is multiplied with the number of inserted/deleted/clipped nucleotides in the read. If the weights are set lower than 1, the share of this type of mismatches in the mismatch score is lower than the number of inserted/deleted/clipped nucleotides. If the weights are set higher than 1, the share of inserted/deleted/clipped nucleotides in the mismatch score is higher than the corresponding number of nucleotides. Users can also set a maximum allowed mismatch score via the option --maximum_mismatch. Reads with a higher mismatch score are discarded, even if they had a lower mismatch score to one of the subgenomes than to the other.

If the read can be mapped onto both subgenomes and the mismatch score of both subgenomes is the same, the read is discarded. All discarded reads are saved in  separate bam files with the suffix 'Ref0.bam'.

**Example 1**: a read is mapped onto subgenome A, but not onto subgenome B. In this case the read is assigned to subgenome A.

**Example 2**: a read is mapped onto subgenome A and subgenome B. However, the read has two substitutions with subgenome A, while it has three substitutions with subgenome B. As no insertions, deletions, or clipped regions are present in the read, the mismatch score of subgenome A and B is equal to two and three, respectively. Consequently, the read is assigned to subgenome A.

**Example 3**: a read is mapped onto subgenome A and subgenome B. However, the read has two substitutions with subgenome A and five soft-clipped nucleotides when mapped onto subgenome A, while it has 3 substitutions with subgenome B and no soft-clipped nucleotides when mapped onto subgenome B. The clipping weight is set to 0.1. The mismatch scores of both subgenomes are calculated as follows:

mismatch score A = 2 (number of point mutations) + 5 * 0.1 (number of clipped nucleotides) = 2.5

mismatch score B = 3 (number of point mutations) + 0 * 0.1 (number of clipped nucleotides) = 3

Consequently, the read is assigned to subgenome A.

**Example 4**: a read is mapped onto subgenome A and subgenome B. However, the read has two substitutions with subgenome A and 10 soft-clipped nucleotides when mapped onto subgenome A, while it has 3 substitutions with subgenome B and no soft-clipped nucleotides when mapped onto subgenome B. The clipping weight is set to 0.1. The mismatch scores of both subgenomes are calculated as follows:

mismatch score A = 2 (number of point mutations) + 10 * 0.1 (number of clipped nucleotides * clipping weight) = 3

mismatch score B = 3 (number of point mutations) + 0 * 0.1 (number of clipped nucleotides * clipping weight) = 3

Because the mismatch scores of both subgenomes are identical, the read cannot be assigned to a subgenome and is discarded.

**Example 5**: a read is mapped onto subgenome A and subgenome B. However, the read has 2 substitutions with subgenome A and 100 soft-clipped nucleotides when mapped onto subgenome A, while it has 3 substitutions with subgenome B and 100 soft-clipped nucleotides when mapped onto subgenome B. The clipping weight is set to 0.1, and the maximum allowed mismatch score is set to 10. The mismatch scores of both subgenomes are calculated as follows:

mismatch score A = 2 (number of point mutations) + 100 * 0.1 (number of clipped nucleotides * clipping weight) = 12

mismatch score B = 3 (number of point mutations) + 100 * 0.1 (number of clipped nucleotides * clipping weight) = 13

The mismatch score of subgenome A is lower than the score of subgenome B, but the read will not be assigned to subgenome A because the mismatch score of subgenome A exceeds the maximum allowed mismatch score (i.e. 10). As a result, the read is discarded.

The script allows for the additional filtering of reads for a minimum mapping score (MAPQ) via the option --mapping_quality. Assigned reads with a lower mapping score than the predefined minimum are discarded. All discarded reads are stored in bam files with the suffix '.Ref0.bam'.

The output bam files with reads assigned to subgenome A and B can be merged via the option --merge_bam, in case the output bam files of both subgenomes need to be processed as one in downstream analyses. The script can also add a dummy read group to the output bam file of each sample (i.e. the two output bam files with reads assigned to each subgenome or, if the --merge_bam option is used, the merged bam file), which is needed for certain SNP callers (e.g. GATK (McKenna et al., 2010)). Installment of Picard (Picard Toolkit, 2019) is required for this option. The bam files with read group are stored in a new subdirectory ('RG') in the output directory. An index file (.bai) is created as well for each new bam file.

Pair-aware mapped forward and reverse reads can be evaluated as separate reads or as a pair of reads. In the latter case, the assignment of reads is based on a combination of the mapping characteristics of the forward and reverse read. The mismatch score of a read pair is calculated as the sum of the mismatch scores of the forward and reverse read. A read pair is assigned to a subgenome if:

1) The number of reads mapped onto one subgenome is higher than the number of reads mapped onto the other subgenome.
2) The number of reads mapped onto both subgenomes is the same, but the mismatch score of one subgenome is lower than the mismatch score of the other.

**Example 1**: the forward and reverse read of a read pair are mapped onto subgenome A, but only the forward read of the read pair is mapped onto subgenome B. As more reads are mapped onto subgenome A than B (2 > 1), the entire read pair is assigned to subgenome A.

**Example 2**: The forward and reverse read can both be mapped onto subgenome A and B. The mismatch scores are calculated as follows:

Subgenome A: 2 (forward) + 3 (reverse) = 5
Subgenome B: 5 (forward) + 1 (reverse) = 6

Even though the reverse read has a lower mismatch score with subgenome B than with subgenome A, the entire read pair is assigned to subgenome A because the total mismatch score (forward + reverse) is lower for subgenome A than for subgenome B.

The number of assigned (uniquely mapped or lowest mismatch score) and unassigned (unmapped, low mapping_quality, equal mismatch score, or maximum mismatch score exceeded) reads of each sample is summarised into a tab-delimited text file, which is stored in the output directory.

#### Usage

`python3 ReadAssignmentTetraploids.py -d_A DIR_A -d_B DIR_B [--pair_aware] [-p PROCESSES] [-q MAPPING_QUALITY] [--clipping_weight] [--indel_weight] [-m MAXIMUM_MISMATCH] [-o OUTPUT_DIRECTORY] [-a SUFFIX_A] [-b SUFFIX_B] [--merge_bam] [--DeleteIntermediateFiles] [--Read_groups]`

**Mandatory argument**

    -d_A, --dir_A  STR  Directory containing unfiltered bam files of read data from tetraploid samples mapped onto subgenome A (same sample set as in -d_B).
    -d_B, --dir_B  STR  Directory containing unfiltered bam files of read data from tetraploid samples mapped onto subgenome B (same sample set as in -d_A).

**Analysis options**

    --pair_aware	             Assign a read pair consisting of a forward and reverse read to a subgenome based on the mapping characteristics of both reads [forward and reverse read in read pairs are separately assigned to a subgenome].
    -p, --processes        INT   Number of processes run in parallel [4].
    -q, --mapping_quality  INT   Minimum mapping quality of a read. Reads with a lower mapping quality are discarded [20].
    --clipping_weight      FLOAT Weight of soft- and hard-clipped nucleotides in the calculation of the mismatch score between the read and a subgenome [1].
    --indel_weight         FLOAT Weight of inserted and deleted nucleotides in the calculation of the mismatch score between the read and a subgenome [1].
    -m, --maximum_mismatch INT   Maximum allowed mismatch score [100].  
    
**Output data options**

    -o, --output_directory  STR   Output directory [current directory].
    -a, --suffix_a          STR   Suffix of output bam files with reads assigned to subgenome A [RefA].
    -b, --suffix_b          STR   Suffix of output bam files with reads assigned to subgenome B [RefB].
    --merge_bam                   Merge the output bam files of both subgenomes [output files not merged].
    --DeleteIntermediateFiles     Delete bam files without secondary alignments and with reads sorted on read ID created by the script [intermediate files not deleted].
    --Read_groups                 Add read groups to the output bam files using Picard and create index file for bam files with read group [read groups not added].    

### 2. FilterVCF.py

The script FilterVCF.py filters a VCF file with biallelic SNPs to obtain a set of genotype calls that fulfill the predefined filter criteria. SNPs in the input VCF file must contain 9 fields (first 9 columns) defining the general characteristics of a SNP (CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, and FORMAT) followed by the genotype calls of each sample. Each genotype call must include the genotype field (GT), the allele depth field (AD, with allele depths separated by a comma), the read depth field (DP), and the genotype quality field (GQ).

The script first evaluates the genotype calls separately. Genotype calls with (an) allele depth(s), a total read depth (sum of allele depths), or a quality score lower than the predefined minima (which are parsed to the script via the options -a, -d, and -q, respectively), are converted into missing data (./.). SNPs with a lower minor allele frequency than the predefined minimum (via the option -m) are discarded as well. The remaining polymorphic SNPs (i.e. SNPs with at least one copy of the reference and alternative allele in the entire sampling) are saved to a new VCF file that is deposited in the output directory. The additional information in the original VCF file, provided on lines starting with a double hash tag (##), and all fields in the genotype calls except the genotype field (GT) are not included in the new VCF file.

The script also allows for the removal of SNPs with a higher proportion of heterozygous genotype calls (0/1) than the predefined maximum (via the option -mh) and/or with a lower proportion of samples without missing data than the predefined minimum (via the -c option). These filtering steps are conducted on the output VCF file created in the first step (see previous paragraph) or on the original VCF file if the filter settings in the previous step were not applied (i.e. the options -a, -d, -q, and -m are left to default). The remaining SNPs are saved to a new VCF file that is stored in the output directory. The additional information in the original VCF file, provided on lines starting with a double hash tag (##), and all fields in the genotype calls except the genotype field (GT) are not included in the new VCF file.

#### Usage

`python3 FilterVCF.py --vcf VCF [-i INPUT_DIRECTORY] [-a AD] [-d DP] [-q GQ] [-m MAF] [-mh MAXIMUM_PROPORTION_HETEROZYGOUS] [-c COMPLETENESS] [-o OUTPUT_DIRECTORY]`

**Mandatory argument**

    --vcf  STR  Name of the input VCF file.
    
**Input data options**

    -i, --input_directory  STR  Input directory containing the input VCF file [default directory].
    
**Analysis options**

    -a, --AD           INT    Minimum allele depth. Genotype calls with a lower allele depth are converted into missing data [0].
    -d, --DP           INT    Minimum total read depth (sum of allele depths). Genotype calls with a lower total read depth are converted into missing data [0].
    -q, --GQ           INT    Minimum genotype quality. Genotype calls with a lower genotype quality are converted into missing data [0].
    -m, --MAF          FLOAT  Minimal minor allele frequency. SNPs with a lower minimal minor allele frequency are discarded [0.0].
    -mh, --maximum_proportion_heterozygous FLOAT  Maximum proportion of heterozygous genotype calls (0/1) in a SNP. SNPs with a higher proportion of heterozygous genotype calls are discarded [1.0].
    -c, --completeness FLOAT  Completeness level of SNPs. SNPs with a lower completeness level are discarded [0.0].
        

**Output data options**

    -o, --output_directory  STR  Output directory [current directory].
    
## References

Li, H., & Durbin, R. (2009). Fast and accurate short read alignment with Burrows–Wheeler transform. Bioinformatics, 25(14), 1754–1760. https://doi.org/10.1093/bioinformatics/btp324

Mckenna, A., Hanna, M., Banks, E., Sivachenko, A., Cibulskis, K., Kernytsky, A., … DePristo, M. A. (2010). The Genome Analysis Toolkit: A MapReduce framework for analyzing next-generation DNA sequencing data. Genome Research, 20(9), 1297–1303. https://doi.org/10.1101/gr.107524.110.20

Picard Toolkit, 2019. Broad Institute, GitHub Repository. http://broadinstitute.github.io/picard/; Broad Institute
